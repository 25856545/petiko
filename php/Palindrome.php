<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        $legth = strlen($word);
        $palavra = [];
        $count = $legth-1;
        for ($i = 0; $i < $legth; $i++){
            $palavra[$i] = $word[$count];
            $count--;
        }
        $polidrome = implode($palavra);
        $polidrome = strtolower($polidrome);
        $polidrome = ucfirst($polidrome);

        $return = strcmp($polidrome, $word);

        if($return == 0){
            return true;
        }else{
            echo false;
        }

    }
}

echo Palindrome::isPalindrome('Deleveled');
